package com.example.pithitamacbook.simpledb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText id,name;
    Button insert, view, update, delete;
    TextView text;
    DBHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id = (EditText) findViewById(R.id.edt_order);
        name = (EditText) findViewById(R.id.edt_name);
        insert = (Button) findViewById(R.id.btn_insert);
        view = (Button) findViewById(R.id.btn_view);
        update = (Button) findViewById(R.id.btn_update);
        delete = (Button) findViewById(R.id.btn_del);
        text = (TextView) findViewById(R.id.text);

        db= new DBHandler(getApplicationContext());

        insert.setOnClickListener(this);
        view.setOnClickListener(this);
        update.setOnClickListener(this);
        delete.setOnClickListener(this);
    }

    public void buttonAction(View view){
        switch (view.getId()){
            case R.id.btn_insert : db.insertRecord(name.getText().toString());
                Toast.makeText(getApplicationContext(), "record inserted", Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_del : db.deleteRecord(id.getText().toString());
                Toast.makeText(getApplicationContext(),"record deleted", Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_view : text.setText(db.getRecord());
                break;
            case R.id.btn_update : db.updateRecord(id.getText().toString(),name.getText().toString());
                Toast.makeText(getApplicationContext(), "record updated" , Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        buttonAction(view);
    }
}
